var express = require('express');
var router = express.Router();

var VKFinder = require('../lib/vk-finder.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  var query = req.query;

  var pass = {
    title: 'VK User Finder',
    menu: 'users',
    query: query || {}
  };

  // console.log('query:', query);

  new Promise((resolve, reject) => {
    if (!query.name) {
      return resolve();
    }

    return VKFinder.findUsers(query)
      .then(firstFound => {

        if (firstFound >= 1000) {
          query.offset = 1000;

          VKFinder.findUsers(query)
            .then(secondFound => {
              resolve(firstFound.concat(secondFound));
            }, reject);
        }

        else {
          resolve(firstFound);
        }
      }, reject);
  })
  .then(found => {
    pass.found = found;
    return res.render('users', pass);
  })
  .catch(err => {
    pass.err = err;
    res.status(400);
    return res.render('users', pass);
  });
});

module.exports = router;
