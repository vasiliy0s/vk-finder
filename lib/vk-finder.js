var request = require('superagent');
var _ = require('lodash');

var VK_API_URL = 'https://api.vk.com/method';
var VK_API_VERSION = '5.25';

var DEFAULT_USER_FIELDS = [
  'followers_count',
  'photo_200',
  'contacts',
  'lists',
  'commont_count',
  'can_write_private_message'
];

function VKFinder() {}

VKFinder.findUsers = function(query) {
  return new Promise((resolve, reject) => {
    if (!query.access_token) {
      return reject('No Access Token provided');
    }

    if (!query.name) {
      return reject('No query string ("name") provided');
    }

    var req = makeRequestTo('users.search')
      .query({ access_token: query.access_token })
      .query({ q: query.name })
      .query({ offset: query.offset || 0 })
      .query({ count: 1000 })
      .query({ fields: (query.fields || DEFAULT_USER_FIELDS).join(',') });

    if (query.age) {
      req = req
        .query({ age_from: query.age })
        .query({ age_to: query.age });
    }

    if (query.city) {
      req = req.query({ hometown: query.city });
    }

    if (query.sex) {
      req = req.query({ sex: query.sex === 'male' ? 2 : 1 });
    }

    req
      .end((err, res) => {
        if (err) {
          throw err;
        }

        if (res.body.error) {
          console.error('VKFinder.findUsers error:', res.body.error)
          return reject(res.body.error.error_msg);
        }

        var response = res.body && res.body.response || {};

        return resolve(response.count ? VKFinder.filtrateWith(query.filter, response.items) : []);
      });
  });
}

VKFinder.filtrateWith = function(filter, collection) {
  var _collection = collection;

  if (!filter) {
    return _collection;
  }

  // followers_count filter.
  if (filter.friends) {
    _collection = _.filter(_collection, { 'followers_count': parseInt(filter.friends) });
  }

  return _collection;
}

function makeRequestTo(method, httpMethod) {
  return request[httpMethod || 'post'](`${VK_API_URL}/${method}`)
      .query({ lang: 'ru' })
      .query({ v: VK_API_VERSION });
}

module.exports = VKFinder;
